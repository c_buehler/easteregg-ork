export const enum Direction {
  North,
  NorthEast,
  East,
  SouthEast,
  South,
  SouthWest,
  West,
  NorthWest
}

export enum Action {
  walk = "walk",
  fight = "fight",
  hit = "hit",
  death = "death"
}
