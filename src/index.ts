import { Ork } from "./ork";

const allowedKeys: { [code: number]: string } = {
  37: "left",
  38: "up",
  39: "right",
  40: "down",
  65: "a",
  66: "b"
};

const konamiCode = [
  "up",
  "up",
  "down",
  "down",
  "left",
  "right",
  "left",
  "right",
  "b",
  "a"
];

let konamiCodePosition = 0;

if (window && window.document && !(window as any).attached) {
  const listener = (e: KeyboardEvent) => {
    const key = allowedKeys[e.keyCode];
    const requiredKey = konamiCode[konamiCodePosition];

    if (key === requiredKey) {
      konamiCodePosition++;
      if (konamiCodePosition == konamiCode.length) {
        try {
          new Ork();
        } catch {
        } finally {
          document.removeEventListener("keydown", listener);
        }
        konamiCodePosition = 0;
      }
    } else {
      konamiCodePosition = 0;
    }
  };

  document.addEventListener("keydown", listener);
  (window as any).attached = true;
}
