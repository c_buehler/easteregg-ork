import { Action, Direction } from "./enums";

const css = require("./ork.scss");

const ORK_ID = "ork_div";
const SPEED = 2;
const MAX_HEALTH = 5;
const UPDATE_INTERVAL = 16;
const RESURRECTION_TIMEOUT = 2000;

export class Ork {
  private health: number = 5;

  private action: Action = Action.walk;
  private direction: Direction = Direction.North;

  private ork: HTMLDivElement | null = null;
  private healthbar: HTMLDivElement | null = null;

  private mouseX: number = document.body.offsetWidth;
  private mouseY: number = document.body.offsetHeight;

  private timeout: number | null = null;

  private get animation(): string {
    return `${this.action}${this.direction}`;
  }

  private get position(): { x: number; y: number } {
    if (!this.ork) {
      return { x: 0, y: 0 };
    }
    return {
      x: this.ork.offsetLeft,
      y: this.ork.offsetTop
    };
  }

  private get nextAction(): Action {
    return this.distance({ x: this.mouseX, y: this.mouseY }) <= 32
      ? Action.fight
      : Action.walk;
  }

  private get nextDirection(): Direction {
    if (!this.ork) {
      return Direction.North;
    }

    const { x, y } = this.position;

    const diffX = (x + 36 - this.mouseX) * -1;
    const diffY = y + 36 - this.mouseY;

    let angle = Math.atan(diffY / diffX);

    if (diffX < 0) {
      angle += Math.PI;
    } else if (diffY < 0) {
      angle += Math.PI * 2;
    }

    switch (true) {
      case !Ork.inBetween(angle, (15 * Math.PI) / 8, Math.PI / 8):
        return Direction.East;
      case Ork.inBetween(angle, Math.PI / 8, (3 * Math.PI) / 8):
        return Direction.NorthEast;
      case Ork.inBetween(angle, (5 * Math.PI) / 8, (7 * Math.PI) / 8):
        return Direction.NorthWest;
      case Ork.inBetween(angle, (7 * Math.PI) / 8, (9 * Math.PI) / 8):
        return Direction.West;
      case Ork.inBetween(angle, (9 * Math.PI) / 8, (11 * Math.PI) / 8):
        return Direction.SouthWest;
      case Ork.inBetween(angle, (11 * Math.PI) / 8, (13 * Math.PI) / 8):
        return Direction.South;
      case Ork.inBetween(angle, (13 * Math.PI) / 8, (15 * Math.PI) / 8):
        return Direction.SouthEast;
      case Ork.inBetween(angle, (3 * Math.PI) / 8, (5 * Math.PI) / 8):
      default:
        return Direction.North;
    }
  }

  constructor() {
    this.ork = document.getElementById(ORK_ID) as HTMLDivElement;

    document.addEventListener("mousemove", (event: MouseEvent) => {
      this.mouseX = event.pageX;
      this.mouseY = event.pageY;
    });

    this.reset();
  }

  private reset(): void {
    if (this.healthbar) {
      this.healthbar.remove();
      this.healthbar = null;
    }

    if (this.ork) {
      this.ork.remove();
      this.ork = null;
    }

    this.health = MAX_HEALTH;

    this.ork = document.createElement("div");
    this.ork.classList.add(css["ork"]);
    this.ork.id = ORK_ID;
    this.ork.addEventListener("click", ev => this.hit(ev));

    this.healthbar = document.createElement("div");
    this.healthbar.classList.add(css["ork-health"]);

    document.body.appendChild(this.ork);
    this.ork.appendChild(this.healthbar);

    this.timeout = setTimeout(() => this.render(), UPDATE_INTERVAL) as any;
  }

  private render(): void {
    this.setAnimation(this.nextAction, this.nextDirection);
    this.walk();

    this.timeout = setTimeout(() => this.render(), UPDATE_INTERVAL) as any;
  }

  private walk(): void {
    if (this.action !== Action.walk || !this.ork) {
      return;
    }

    const { x, y } = this.position;
    const diffX = (x + 36 - this.mouseX) * -1;
    const diffY = y + 36 - this.mouseY;

    let angle = Math.atan(diffY / diffX);

    if (diffX < 0) {
      angle += Math.PI;
    } else if (diffY < 0) {
      angle += Math.PI * 2;
    }

    const distance = Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2));

    const step = Math.min(SPEED, distance);

    const newX = step * Math.cos(angle);
    const newY = step * Math.sin(angle) * -1;

    this.ork.style.left = `${x + newX}px`;
    this.ork.style.top = `${y + newY}px`;
  }

  private die(): void {
    this.setAnimation(Action.death, this.direction);
    setTimeout(() => this.reset(), RESURRECTION_TIMEOUT);
  }

  private setAnimation(nextAction: Action, nextDirection: Direction): void {
    const newAnimation = `${nextAction}${nextDirection}`;

    if (this.animation !== newAnimation && this.ork) {
      this.ork.classList.remove(css[this.animation]);
      this.ork.classList.add(css[newAnimation]);
      this.action = nextAction;
      this.direction = nextDirection;
    }
  }

  private hit({ pageX, pageY }: MouseEvent): void {
    if (this.health <= 0 || !this.healthbar) {
      return;
    }

    if (this.distance({ x: pageX, y: pageY }) <= 30) {
      if (this.timeout) {
        clearTimeout(this.timeout);
        this.timeout = null as any;
      }

      this.setAnimation(Action.hit, this.direction);
      this.health--;

      const newHp = 100 - 100 * (this.health / MAX_HEALTH);
      this.healthbar.style.width = `${newHp}%`;

      if (this.health <= 0) {
        this.die();
      } else {
        this.timeout = setTimeout(() => this.render(), 200) as any;
      }
    }
  }

  private distance({ x, y }: { x: number; y: number }): number {
    if (!this.ork) {
      return 0;
    }

    const { x: orkX, y: orkY } = this.position;

    const diffX = (orkX + 36 - x) * -1;
    const diffY = orkY + 36 - y;

    return Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2));
  }

  private static inBetween(number: number, min: number, max: number): boolean {
    const lowest = Math.min(min, max);
    const highest = Math.max(min, max);
    return number > lowest && number < highest;
  }
}
