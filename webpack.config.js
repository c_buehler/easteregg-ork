const { CheckerPlugin } = require('awesome-typescript-loader');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { join } = require('path');

module.exports = (_, { mode }) => {
  const isProduction = mode === 'production';
  const output = join(__dirname, 'build');

  return {
    mode,
    watch: !isProduction,
    devtool: isProduction ? '' : 'source-map',
    entry: {
      ork: require.resolve('./src/index.ts'),
      demo: require.resolve('./src/demo.ts'),
    },
    output: {
      publicPath: './',
      path: output,
      pathinfo: !isProduction,
      filename: 'js/[name].js',
    },
    devServer: {
      contentBase: join(__dirname, 'build'),
      compress: true,
      port: 8080,
      stats: 'minimal',
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    module: {
      rules: [
        {
          oneOf: [
            {
              test: /\.tsx?$/,
              loader: 'awesome-typescript-loader',
            },
            {
              test: /\.scss$/,
              use: [
                'style-loader',
                {
                  loader: 'css-loader',
                  options: {
                    modules: true,
                  },
                },
                'sass-loader',
              ],
            },
            {
              exclude: [/\.(j|t)sx?$/, /\.html$/, /\.json$/],
              use: ['url-loader'],
            },
          ],
        },
      ],
    },
    plugins: [
      new CleanWebpackPlugin(output),
      new CheckerPlugin(),
      new HtmlWebpackPlugin({
        template: 'src/index.html',
        minify: isProduction,
      }),
    ],
  };
};
